
type term = Const of string
	    | Num of int
	    | Var of string
	    | App of term * term
	    | Abs of string * term

type env = (string, term) Hashtbl.t

module SS = Set.Make (String)

let ( $$ ) f x = f x 

(* Pretty print *)

let rec pprint = function
  | Const s -> s
  | Num n -> string_of_int n
  | Var s -> s
  | App (a, b) -> "(" ^ pprint a ^ " " ^ pprint b ^ ")"
  | Abs (x, t) -> "\\" ^ x ^ "." ^ pprint t

(* Fresh variable names generation, for α-conversion *)

let current_name = ref 0 

let reset () = current_name := 0; ()

let fresh () = incr current_name; 
  "v#" ^ string_of_int !current_name

(* Replace var1 by var2 in a term *) 

let rec alpha_conv t var1 var2 = 
  match t with
  | Var v -> if v = var1 then Var var2 else t
  | App (a, b) -> App (alpha_conv a var1 var2, alpha_conv b var1 var2)
  | Abs (v, tt) -> if v = var1 
    then Abs (var2, alpha_conv tt var1 var2)
    else Abs (var1, alpha_conv tt var1 var2)
  | _ -> t

(* Get the set of the free variables of a term *)

let rec free_vars t =
  match t with
  | Var v -> SS.singleton v
  | App (a, b) -> SS.union (free_vars a) (free_vars b)
  | Abs (v, tt) -> SS.remove v (free_vars tt)
  | _ -> SS.empty

(* Substitution : t[x := u] *)

let rec subst t x u =
  match t with
  | Var v -> if x = v then u else t
  | App (a, b) -> App (subst a x u, subst b x u)
  | Abs (z, tt) -> 
    if x = z then t
    else if SS.mem z (free_vars u) then
      let n = fresh () in
      Abs (n, subst (alpha_conv tt z n) x u)
    else Abs (z, subst tt x u)
  | _ -> t

(* Left reduction *)

let rec reduce t = 
  match t with
  | Abs (x, tt) -> Abs (x, reduce tt)
  | App (Abs (x, tt), z) -> reduce $$ subst tt x z
  | App (a, b) -> reduce $$ App (reduce a, reduce b)
  | _ -> t

(* Lambda term parsing *)
(*
let lexer = Genlex.make_lexer ["\\"; "."; "("; ")"; "$"]

let parse = parser
  | [< 'Int n >] -> Num n
  | [< 'Kwd "\\"; 'Ident x; 'Kwd "."; t = parse >] -> Abs (x, t)
  | [< 'Kwd "("; a = parse; 'Kwd "$"; b = parse ; 'Kwd ")" >] ->
    App (a, b)

let test = parse $$ lexer (Stream.of_string "\\x. (x x)")
*)

let parse_term _ = None


(* Environment printing *)

let print_env e =
  if Hashtbl.length e = 0 
  then print_string "The current environment is empty.\n"
  else Hashtbl.iter (fun k t ->  print_string $$ k ^ " = " ^ pprint t ^ "\n") e;
  ()

(* Input checking *)

let is_cst s = Str.string_match (Str.regexp "[A-Z][a-zA-Z-]*") s 0

let is_id s = Str.string_match (Str.regexp "[a-z][a-zA-Z-]*") s 0

(* Toplevel *)

let _ =
  let v_exit = ref false in
  let input = ref "" in
  let environment = (Hashtbl.create 30 : env) in
 
  let error s = print_string $$ s ^ "\n" in

  while not !v_exit do
    
    print_string "PLC ? ";
    
    input := read_line ();
    
    match !input with
    | "Salut" -> v_exit := true;
    | "Env" -> print_env environment;
    | s -> 
      let splitted = Str.split (Str.regexp " ") s in
      ( match splitted with
      | "Def" :: rest -> 
	let decl = Str.split (Str.regexp "=") (List.fold_left (^) "" rest) in
	( match decl with
	| [cst; term] -> 
	  if not (is_cst cst) 
	  then error "Wrong constant name."
	  else match (parse_term term) with
	  | None -> error "Wrong term."
	  | Some t -> Hashtbl.replace environment cst t;
	    print_string $$ cst ^ " has been added to the environment.\n"
	| _ -> error "Wrong declaration form." )
      | "Reduit" :: term -> ()
      | "Redex" :: term -> ()
      | "Redn" :: n :: term -> ()
      | "Affiche" :: cst -> () )
  done
